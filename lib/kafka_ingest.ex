defmodule KafkaIngest do
  @moduledoc false

  use GenServer

  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default)
  end

  def init(_opts) do

    lines = Unit.sample_log_lines(30_000)
    #timer = Process.send_after(self(), :work, 1_000)
    timer = nil

    {:ok, %{timer: timer, lines: lines, start: NaiveDateTime.utc_now(), count: 0}}
  end


  def handle_info(:work, state) do
    # Do the work you desire here

    # emit lines to available parser worker

    #IO.puts "ingest event"

    GenServer.call(RFC5424Parser, {:parse, state.lines})

    # Start the timer again
    timer = Process.send_after(self(), :work, 1)
    state = Map.put(state, :timer, timer)

    {:noreply, state}
  end


end