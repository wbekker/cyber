defmodule InfluxConnection do
  use Instream.Connection, otp_app: :cyber


  def write_eps(events_per_second) do
    %{
      points:
        [
        %{
          database: "cyber",
          measurement: "events_per_second",
          tags: %{
            foo: "foo",
            bar: "bar"
          },
          fields: %{
            value: events_per_second
          }
        }
      ],
       database: "cyber"
    }
    |> InfluxConnection.write(log: false)

  end
end
