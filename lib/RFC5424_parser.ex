defmodule RFC5424Parser do
  @moduledoc false

  use GenServer

  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default, name: RFC5424Parser)
  end

  def init(_opts) do
    {:ok, %{}}
  end

  def handle_call({:parse, lines}, _from, state) do

    events = Unit.parse(lines)

    #IO.puts "parse event"

    # TODO: slice events per time batch and trigger cast per batch
    GenServer.call(Enricher, {:enrich, events})

    {:reply, :ok, state}
  end


end