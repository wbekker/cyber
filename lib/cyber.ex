defmodule Cyber do
  use Application

  def start(_type, _args) do
    children = [KafkaIngest, RFC5424Parser, Enricher, CardinalityProfiler, InfluxConnection]
    Supervisor.start_link(children, strategy: :one_for_all)
  end
end
