defmodule Enricher do
  @moduledoc false

  use GenServer

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: Enricher)
  end

  def init(_opts) do
    {:ok, %{}}
  end

  def handle_call({:enrich, events}, _from, state) do
    #IO.puts "enrich event"

    # simple key value enrich
    enriched_events = Unit.enrich(events)

    # profile enrich

    GenServer.call(CardinalityProfiler, {:profile, enriched_events})

    {:reply, :ok, state}
  end
end