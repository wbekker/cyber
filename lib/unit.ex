defmodule Unit do
  @moduledoc false


  def work() do
    lines = sample_log_lines(100_000)
    events = parse(lines)
    enrich(events)
    :ok
  end

  def sample_log_lines(nr) do
    syslog_line = "<34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - - - 'su root' failed for lonvick on /dev/pts/8"
    (1 .. nr) |> Enum.map(fn _i -> syslog_line end)
  end

  def parse(lines) do
    {:ok, regex} = regex()
    lines |> Enum.map(fn line -> Regex.named_captures(regex, line) end)
  end

  def enrich(events) do
    events |> Enum.map(fn event -> Map.put(event, :fake_enrichment, "foo") end)
  end

  def regex() do
    Regex.compile("\\<*(?<PRI>\\d+)\\>*(?<VERSION>\\d+) *(?<TIMESTAMP>\\b\\S+\\b) *(?<HOSTNAME>\\b\\S+\\b) *(?<APPNAME>\\b\\S+\\b) *(?<PROCID>\\b\\S+\\b) *(?<MSGID>[\\x21-\\x7E]*) *(?<SD>\\[.*\\])* *(?<MSG>.*)")
  end
end
