defmodule CardinalityProfiler do
  @moduledoc false

  use GenServer

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: CardinalityProfiler)
  end

  def init(_opts) do
    {:ok, %{ start: NaiveDateTime.utc_now(), count: 0}}
  end

  def handle_call({:profile, events}, _from, state) do

    #IO.puts "profile event"

    # events need to be grouped by time window.
    # get hllp plus structure for time segment (new or create)
    # apply new values
    # persist updated hllp

    #events |> Enum.map(fn event ->    end)

    state = Map.put(state, :count, state.count + length(events))

    InfluxConnection.write_eps(throughput(state.start, state.count))

    {:reply, :ok, state}
  end

  defp throughput(start, count) do
    Integer.floor_div(count,NaiveDateTime.diff(NaiveDateTime.utc_now(), start, :second))
  end
end