defmodule Config do
  use Mix.Config

  config :cyber, InfluxConnection,
         database:  "cyber",
         host: "localhost",
         http_opts: [insecure: true],
         pool: [max_overflow: 10, size: 50],
         port: 8086,
         scheme: "http",
         writer: Instream.Writer.Line

end
